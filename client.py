import os
import lexEngine.lexengine as LE
import subprocess
import nltk
import requests
from urlparse import urlparse
import random
import bs4
import urllib2
import shutil
import re
import bs4 as BeautifulSoup
import unicodedata
import twitterDicts
from urllib import urlopen
class client:
	industry = ""
	url = ""
	clientName = ""
	industryWords = []
	domain = ""
	workingDirectory = ""
	htmlText = ""
	pdfsDownloaded = 0
	def __init__(self, industry, url, clientName, domain, directory):
		self.industry = industry
		self.url = url
		self.clientName = clientName
		if not os.path.exists("data/industries/"+industry+'/'+clientName): os.makedirs("data/industries/"+industry+'/'+clientName)
		if not os.path.exists("data/industries/"+industry+'/'+clientName+'/html'): os.makedirs("data/industries/"+industry+'/'+clientName+'/html')
		self.domain = domain
		self.workingDirectory = directory
		self.recursionLevels = 2

	def loadRelevantIndustry(self):
		try:
			f = open("/data/industries"+industry)
			read = f.readlines()
			for line in read:
				industryWords.append(line.replace('\n'))
		except Exception, e:
			pass
		finally:
			f.close

	def visible(self,element):
	    try:
		    if element.parent.name in ['style', 'script', '[document]', 'head', 'title']:
			return False
		    elif re.match('<!--.*-->', str(element)):
			return False
		    return True
	    except:
		    return False
	def crawl(self):
		try:
			os.remove("outputhtml")
		except:
			pass
		print "[+] Crawling..."
		x = subprocess.Popen(['linkchecker',self.url, "-r", "{0}".format(self.recursionLevels)], stdout=subprocess.PIPE)
		
		output = x.stdout.readlines()
		urlLines = []
		print "[+] Done Crawling"
		print "[+] Beginning information extraction"
		for line in output:
			if "Real URL" in line:
				url = line[11:]
				url = url.lower()
				notinterested = [".png", ".jpg",".js",".css", ".txt", ".zip", ".mp3", "jquery"]
				wanted = True
				for linkType in notinterested:
					if linkType in url:
						wanted = False
				if wanted:
					if url[len(url)-5:len(url)-1] in notinterested:
						print url[len(url)-5:len(url)-1] + " not wanted"
						continue
					urlLines.append(url)
		finalOutputText = ""
		pagesAnalysed = 0
		for url in urlLines:
			#print url
			#if self.domain not in url:
				#print "Skipping " + url
			#	continue
			if ".pdf" in url:
				if self.pdfsDownloaded < 6:
					#download and extract text
					try:
						finalOutputText += self.extractInfoFromPdf(url)
						self.pdfsDownloaded = self.pdfsDownloaded + 1
					except Exception, e:
						print e
						pass
				else:
					continue
			parsed_uri = urlparse(url)
			URLDomain = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
			if (self.domain in URLDomain):
			#try:
				#print self.domain + " - " + url
				html = urlopen(url).read()
				soup_text = bs4.BeautifulSoup(html)
			
				texts = soup_text.findAll(text=True)					
				visible_texts = filter(self.visible, texts)
				raw = ""
				for i in visible_texts:
					raw+=self.santise(i)
				finalOutputText += str(raw)
				pagesAnalysed += 1
				print "[-] Extracted {0} articles for analysis.".format(pagesAnalysed)
			#except:
				#print ("[!] Could not download "+url)
			#	pass
			else:
				pass
				#print (URLDomain + "not in "+self.domain)
			

		print "[-] Finished and gathering text"
		self.htmlText = self.santise(finalOutputText)
		f = open(self.workingDirectory + 'crawledText', 'w')
		f.write(self.htmlText)
		f.close()
		print "[-] Done crawling"
	def santise(self, text):
		#Removes any weird noise from the text
		santised = ""
		text = text.replace("\n", " ")
		text = text.replace("\t", " ")
		for c in text:
			allowed = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
			if c in allowed:
				santised += c
			else:
				santised += " "
		return santised
		
	def extractInfoFromPdf(self, url):
		try:
			print "[-] Found PDF for text extraction\n[-] Downloading..."
			filename = str(random.randint(0,1500000))
			outputFileName = self.workingDirectory + filename + ".pdf"
			f = urllib2.urlopen(url)
			with open(outputFileName, "wb") as code:
				code.write(f.read())
			#Now we have downloaded the pdf. Yay. Now we need to extract the text from it.
			x = subprocess.Popen(['ps2ascii', outputFileName], stdout=subprocess.PIPE)
			print "[-] Downloaded pdf. Will only download {0} more".format(5 - self.pdfsDownloaded)
			return x.stdout.read()
		except Exception, e:
			print "[-] PDF not successfully downloaded"
			print e
			return ""

	def buildDictionaryText(self, fileLoc):
		fileLoc = fileLoc.strip()
		if fileLoc[0] == '\'' and fileLoc[len(fileLoc)-1] == '\'':
			fileLoc = fileLoc[1:len(fileLoc)-1]
		while(not(os.path.exists(fileLoc))):
			print "[-] The file location entered does not seem to exist:\n{0}.\nTry again.".format(fileLoc)
			fileLoc = raw_input()
		try:
			f = open(fileLoc)
			text = f.read()
			f.close()
		except:
			print "[-] Could not read file."
		try:
			lex = LE.lexengine(text, self.workingDirectory+"TxtDictionary.txt")
			print "[+] Beginning analysis..."
			lex.trimPercentage()
		except:
			print "[-] Could not process file."


	def buildDictionaryPdf(self, fileLoc):
		fileLoc = fileLoc.strip()
		
		if fileLoc[0] == '\'' and fileLoc[len(fileLoc)-1] == '\'':
			fileLoc = fileLoc[1:len(fileLoc)-1]
		while(not (os.path.exists(fileLoc))):
			print "[-] The file location entered does not seem to exist:\n{0}.\nPlease reenter the path.".format(fileLoc)
			fileLoc = raw_input()
	#try:
		x = subprocess.Popen(['ps2ascii', fileLoc], stdout=subprocess.PIPE)
		print "[-] Extracting text from pdf."
		lex = LE.lexengine(x.stdout.read(), self.workingDirectory+"PdfDictionary.txt")
		
		print "[+] Beginning analysis..."
		lex.trimPercentage()
	#except:
		#print "[-] Could not process file."


	def buildDictionary(self, rLevels):
		self.recursionLevels = rLevels
		self.crawl()
		lex = LE.lexengine(self.htmlText, self.workingDirectory+"WebsiteDictionary.txt")
		print "[+] Beginning trim..."
		lex.trimPercentage()
	
	def buildAggregate(self):
		lex = LE.lexengine("", self.workingDirectory+"AggregateDictionary.txt", False)
		
		dirname = self.workingDirectory
		currDicts = []
		if os.path.exists(self.workingDirectory + 'PdfDictionary.txt'):
			currDicts.append(self.workingDirectory+"PdfDictionary.txt")
		if os.path.exists(self.workingDirectory + 'TxtDictionary.txt'):
				currDicts.append(self.workingDirectory+"TxtDictionary.txt")
		if os.path.exists(self.workingDirectory + 'WebsiteDictionary.txt'):
				currDicts.append(self.workingDirectory+"WebsiteDictionary.txt")
		if os.path.exists(self.workingDirectory + 'TwitterHandleDictionary.txt'):
			currDicts.append(self.workingDirectory+"TwitterHandleDictionary.txt")
		if os.path.exists(self.workingDirectory + 'TwitterSearchTermDictionary.txt'):
			currDicts.append(self.workingDirectory+"TwitterSearchTermDictionary.txt")
		lex.aggregateDict(currDicts)

	def buildDictionaryFromTwitterUsername(self, handle):
		t = twitterDicts.twitterSearch()
		lex = LE.lexengine(t.searchByUser(handle), self.workingDirectory+"TwitterHandleDictionary.txt")
		print "[+] Beginning trim..."
		lex.trimPercentage()
	def buildDictionaryFromTwitterSearchTerm(self, term):
		t = twitterDicts.twitterSearch()
		print "How many tweets would you like to analyse?:(Default = 700) (Max = 700)"
		count = int(raw_input())
		lex = LE.lexengine(t.searchByTerm(term, count), self.workingDirectory+"TwitterSearchTermDictionary.txt")
		print "[+] Beginning trim..."
		lex.trimPercentage()
